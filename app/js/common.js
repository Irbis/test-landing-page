$(function () {
	const controller = new ScrollMagic.Controller();

	//
	//header
	//
	const timelineHeader = new TimelineMax();

	timelineHeader
		.fromTo('.loader__inside', 1, {scale: 0.01}, {
			scale: 1, onComplete: function () {
				$('.loader').remove();
			}
		})
		.fromTo('.header .logo__word', .7, {y: 100}, {y: -4}, "-=.7")
		.staggerFromTo('.navigation li', 1, {y: 100, opacity: 0, scale: 0}, {y: 0, opacity: 1, scale: 1}, 0.25, "<")
		.from('.header .logo_x_first, .header .logo_x_second', 1.2, {y: 100, opacity: 0}, "<")
		.to('.logo__word', .6, {y: 0, ease: Bounce.easeInOut}, "-=1")
		.fromTo('.header__contacts .phone, .header__contacts .email', .9, {y: -60}, {y: 0}, "-=1")

	const headerScene = new ScrollMagic.Scene({
		triggerElement: '.header',
		duration      : 0,
		triggerHook   : 0,
	})
		.setTween(timelineHeader)
		.addTo(controller);
	//
	// header end

	//
	// top banner
	//
	const topBannerParallax = TweenMax.to($(".js-top-banner-background .js-top-banner-background-img"), 1, {
		opacity: 0,
		scale: 1.5
	});

	const topBannerScene = new ScrollMagic.Scene({
		triggerHook: 0,
		triggerElement: ".top-banner",
		duration: '150%'
	})
		.setTween(topBannerParallax)
		.addTo(controller);

	const topBannerTitleScene = new ScrollMagic.Scene({
		triggerHook: 0,
		triggerElement: ".top-banner",
		duration: '150%'
	})
		.setTween(TweenMax.staggerTo($(".js-top-banner-content-item"), 1, {
			y: '200%',
			opacity: 0
		}))
		.addTo(controller);
	//
	// top banner end


	//
	// feedback sliders
	//
	$('.owl-carousel').owlCarousel({
		loop      : true,
		margin    : 60,
		navText   : ["<img src='./img/back.svg'>", "<img src='./img/back.svg'>"],
		responsive: {
			0   : {
				margin: 30,
				items : 1,
				nav   : false,
				dots  : false,
			},
			580 : {
				items: 1,
				nav  : false,
				dots : false,
			},
			768 : {
				items: 1,
				nav  : false,
				dots : false,
			},
			1000: {
				items: 1,
				nav  : true,
				dots : false,
			}
		}
	});
	//
	// feedback slider end


	//
	// handler submit order form
	//
	$(document).on('submit', '.js-form-order-test', function ( e ) {
		e.preventDefault();
		const formSerialize = $(this).serialize();

		// test alert
		alert(formSerialize);
		$(this).find('button.close').trigger('click');

		return true;
		// test alert end

		$.ajax({
			type    : 'POST',
			url     : 'end-point/order-test',
			dataType: 'json',
			data    : formSerialize,
			success : res => {
				console.log(res);
				// do

				return true
			},
			error   : err => {
				console.error(err)
			}
		});

		return false;
	});
	//
	// handler submit order form end


	//
	// flowing scroll to anchor
	//
	$('.js-flowing-scroll').on('click', function(){
		const el = $(this);
		const dest = el.attr('href'); // получаем направление
		if(dest !== undefined && dest !== '') { // проверяем существование
			$('html').animate({
					scrollTop: $(dest).offset().top // прокручиваем страницу к требуемому элементу
				}, 500 // скорость прокрутки
			);
		}
		return false;
	});
	//
	// flowing scroll to anchor end


	//
	// hover popular question
	//
	$(".js-popular-questions-scene-item").hover(function() {
		const parent = $(this).closest('.js-popular-questions-scene');
		const items = parent.find('.js-popular-questions-scene-item');
		const parentContentBlock = parent.siblings('.js-popular-questions-card');
		const itemsContent = parentContentBlock.find('.js-popular-questions-card-item');
		const indexHoverItem = $(this).index();

		items.each(function ( index ) {
			if( index === indexHoverItem ) {
				$(this).addClass('active');
				$(itemsContent).eq(index).addClass('active');
			} else {
				$(this).removeClass('active');
				$(itemsContent).eq(index).removeClass('active');
			}
		});
	});
	//
	// hover popular question end
});
